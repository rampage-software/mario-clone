﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailScript : MonoBehaviour
{
    public float movementSpeed = 1.0f;
    private Rigidbody2D rig;
    private Animator anim;

    private bool moveLeft;
    public Transform downCollision;

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        moveLeft = true;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        if (moveLeft)
        {
            rig.velocity = new Vector2(-movementSpeed, rig.position.y);
        }
        else
        {
            rig.velocity = new Vector2(movementSpeed, rig.position.y);
        }
        CheckDirection();
    }

    void CheckDirection()
    {
        if (!Physics2D.Raycast(downCollision.position, Vector2.down, 0.1f))
        {
            ChangeDirection();
        }

    }

    void ChangeDirection()
    {
        moveLeft = !moveLeft;

        Vector3 tempScale = transform.localScale;
        if (moveLeft)
        {
            tempScale.x = Mathf.Abs(tempScale.x);
        }
        else
        {
            tempScale.x = -Mathf.Abs(tempScale.x);
        }

        transform.localScale = tempScale;


    }


}
