﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float playerSpeed = 5f;
    private Rigidbody2D rig;
    private Animator anim;

    private bool isGrounded;
    private bool jumped;

    public float jumpPower;

    public Transform groundCheck;
    public LayerMask groundLayer;

    void Awake()
    {
        anim = GetComponent<Animator>();
        rig = GetComponent<Rigidbody2D>();
    }

    //public void Update()
    //{
    //    if (Physics2D.Raycast(groundCheck.position, Vector2.down, 0.5f, groundLayer))
    //    {
    //        print("Collided with ground using raycast");
    //    }
    //}

    void Update()
    {
        CheckIfGrounded();
        PlayerJump();
    }

    void FixedUpdate() => PlayerWalk();

    void PlayerWalk()
    {
        float movementInput = Input.GetAxisRaw("Horizontal");

        if (movementInput > 0)
        {
            rig.velocity = new Vector2(playerSpeed, rig.velocity.y);
            ChangeDirection(1);
        }
        else if (movementInput < 0)
        {
            rig.velocity = new Vector2(-playerSpeed, rig.velocity.y);
            ChangeDirection(-1);
        }
        else
        {
            rig.velocity = new Vector2(0f, rig.velocity.y);
        }

        anim.SetInteger("Speed", Mathf.Abs((int)rig.velocity.x));
    }

    void PlayerJump()
    {
        if (isGrounded)
        {
            if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow))
            {
                jumped = true;
                rig.velocity = new Vector2(rig.velocity.x, jumpPower);
                anim.SetBool("Jump", true);
            }
        }
    }

    void CheckIfGrounded()
    {
        isGrounded = Physics2D.Raycast(groundCheck.position, Vector2.down, 0.1f, groundLayer);
        if (isGrounded)
        {
            if (jumped)
            {
                jumped = false;
                anim.SetBool("Jump", false);
            }
        }

    }

    void ChangeDirection(int direction)
    {
        Vector3 tempScale = transform.localScale;
        tempScale.x = direction;
        transform.localScale = tempScale;

    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Ground"))
    //    {
    //        print("Collided with ground");
    //    }
    //}

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("Ground"))
    //    {
    //        print("Collided with ground");
    //    }
    //}

} // PlayerMovement
